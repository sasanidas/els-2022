(defcommand forward-sexp ((&optional (arg 1))
                          :prefix)
  (if *forward-sexp-function*
      (funcall *forward-sexp-function* arg)
      (progn
        (goto-char (or (scan-sexps (point) arg) (buffer-end arg)))
        (if (< arg 0) (backward-prefix-chars)))))

*forward-sexp-function*

*current-buffer*





 (progn
        (goto-char (or (scan-sexps (point) arg) (buffer-end arg)))
        (if (< arg 0) (backward-prefix-chars)))))